import requests
from flask_cors import CORS, cross_origin
from flask import Flask, jsonify, request, make_response
from backend_services.tokens.auth import GetParams

get_params = GetParams()
param_names = [
    'COGNITO_APP_CLIENT_ID',
    'COGNITO_POOL_ID',
    'COGNITO_REGION_NAME',
]
param_values = get_params.get_ssm_parameters(param_names)
print(param_values)

COGNITO_POOL_ID = param_values['COGNITO_POOL_ID']
REGION_NAME = param_values['COGNITO_REGION_NAME']
COGNITO_APP_CLIENT_ID = param_values['COGNITO_APP_CLIENT_ID']


app = Flask(__name__)
CORS(app, origins=["http://localhost:3000"], supports_credentials=True)  # get rid in prod
# CORS(app, resources={r"/generate-image/*": {"origins": "http://localhost:3001/"}})



@app.route('/logout', methods=['POST'])
def logout():
    resp = make_response(jsonify(message="Logged out successfully"))
    # Clear cookies by setting them to empty with expiration in the past
    resp.set_cookie('id_token', '', expires=0)
    resp.set_cookie('access_token', '', expires=0)
    resp.set_cookie('refresh_token', '', expires=0)

    return resp


# @app.route('/test', methods=['POST'])
# def test():
#     print('hello')
#     return jsonify(error="you smell"), 400


@app.route('/exchange-code', methods=['POST'])
@cross_origin(origin='http://localhost:3000/login')
def exchange_code():
    data = request.json
    code = data.get('code')
    # print("data", data)
    # Exchange the code for tokens with Cognito
    tokenEndpoint = 'https://doodl.auth.eu-west-2.amazoncognito.com/oauth2/token'
    details = {
        'grant_type': 'authorization_code',
        'client_id': COGNITO_APP_CLIENT_ID,
        'code': code,
        'redirect_uri': 'http://localhost:3000/login',
        # 'client_secret': '<your_client_secret>',  # If you set a client secret in Cognito
    }
    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    response = requests.post(tokenEndpoint, data=details, headers=headers)
    tokens = response.json()
    print("tokens", tokens)
    # Check if the tokens are valid
    if 'access_token' in tokens:
        resp = make_response(jsonify(success=True))

        # Set tokens as regular cookies
        resp.set_cookie('id_token', tokens['id_token'], httponly=True, samesite='Lax')  # Add `secure=True` if using HTTPS
        resp.set_cookie('access_token', tokens['access_token'], httponly=True, samesite='Lax')  # Add `secure=True` if using HTTPS
        resp.set_cookie('refresh_token', tokens['refresh_token'], httponly=True, samesite='Lax')  # Add `secure=True` if using HTTPS
        resp.set_cookie('help', 'checkkkk')

        return resp
    else:
        return jsonify(error="Error exchanging code for tokens"), 400


if __name__ == '__main__':
    app.run(debug=True, port=5200)


# -- AWS LAMBDA FUNCTION --
# def lambda_handler(event, context):
#     # Convert the Lambda event to a WSGI request
#     with app.test_request_context(path=event['path'], 
#                                   query_string=event.get('queryStringParameters'),
#                                   method=event['httpMethod'],
#                                   headers=event['headers'],
#                                   data=event.get('body')):
#         # Flask pre process request
#         response = app.preprocess_request()

#         # If no response is generated in preprocess, dispatch request to view function
#         if response is None:
#             response = app.dispatch_request()

#         # Flask post process request
#         response = app.make_response(response)
#         response = app.process_response(response)

#         # Convert Flask response to Lambda response format
#         lambda_response = {
#             "statusCode": response.status_code,
#             "headers": dict(response.headers),
#             "body": response.get_data(as_text=True)
#         }

#         return lambda_response
