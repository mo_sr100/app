import requests
import openai
from openai import OpenAI
import awsgi
# from wtforms import Form, StringField, validators
from flask_cors import CORS
import html
import ast
# import boto3
# from botocore.exceptions import ClientError
from io import BytesIO
import base64
# from flask import g
from flask import Flask, jsonify, request
# from auth import token_required
# from ..tokens.auth import token_required
import random
import time
from pydantic import BaseModel, ValidationError
from typing import Optional, Literal
from auth import TokenHandler
from auth import GetParams


app = Flask(__name__)
token_handler = TokenHandler()
CORS(app, origins=["http://localhost:3000"], supports_credentials=True)  # get rid in prod
# CORS(app, resources={r"/generate-image/*": {"origins": "http://localhost:3001/"}})


get_params = GetParams()
param_names = ['image_api_key']
param_values = get_params.get_ssm_parameters(param_names)
image_api_key = param_values['image_api_key']
client = OpenAI(api_key=image_api_key)


def retry_with_exponential_backoff(
    func,
    initial_delay: float = 1,
    exponential_base: float = 2,
    jitter: bool = True,
    max_retries: int = 10,
    errors: tuple = (openai.RateLimitError,),
):
    """Retry a function with exponential backoff."""

    def wrapper(*args, **kwargs):
        print('backoff initiated')
        num_retries = 0
        delay = initial_delay
        while True:
            print({'number of retries': num_retries})
            try:
                return func(*args, **kwargs)
            except errors as e:
                num_retries += 1
                if num_retries > max_retries:
                    raise Exception(
                        f"Maximum number of retries ({max_retries}) exceeded."
                    )
                delay *= exponential_base * (1 + jitter * random.random())
                print({'delay': delay})
                time.sleep(delay)
            except Exception as e:
                raise e

    return wrapper


class ImageData(BaseModel):
    prompt: str  # Ensure prompt is a string
    orientation: Optional[Literal["Landscape", "Portrait", "Square"]] = "Square"


@app.route('/generate-image', methods=['GET'])
# @token_handler.token_required
@retry_with_exponential_backoff
def generate_image():
    prompt = request.args.get('input')
    if not prompt:
        return jsonify({"error": "Prompt is required"}), 400
    orientation = request.args.get('orientation', default="Square")
    print(prompt)
    print(orientation)
    sanitized_prompt = html.escape(prompt)
    try:
        validated_data = ImageData(prompt=sanitized_prompt, orientation=orientation)
        data = validated_data.model_dump()
        # return jsonify({"message": "Data is valid", "data": validated_data.model_dump()})
        print(data)
    except ValidationError as e:
        # Handle validation errors
        return jsonify({"error": e.errors()}), 400

    mappings = {
        'Square': '1024x1024',
        'Landscape': '1792x1024',
        'Portrait': '1024x1792',
    }

    if data['orientation'] in mappings:
        shape = mappings[data['orientation']]
    else:
        shape = '1024x1024'

    # response = requests.post(OPENAI_API_URL, headers=HEADERS, json={"prompt": prompt})

    response = client.images.generate(
        model="dall-e-3",
        prompt=data['prompt'],
        size=shape,
        quality="standard",
        n=1,
    )
    # ['256x256', '512x512', '1024x1024', '1024x1792', '1792x1024'] - horizontal x vertical

    dalle_two = [image_url.url for image_url in response.data]

    response = client.images.generate(
        model="dall-e-3",
        prompt=data['prompt'],
        size=shape,
        quality="standard",
        n=1,
    )

    dalle_three = [image_url.url for image_url in response.data]

    # if response.status_code == 429:
    #     print(response)

    # image_url = response.data[0].url

    image_data_list = dalle_two + dalle_three

    # # Step 2: Fetch image data from the URLs
    # image_data_list = [BytesIO(requests.get(image['url']).content) for image in response['data']]

    # # Step 3: Convert image data to base64 format
    # base64_encoded_images = []

    # for image_bytes in image_data_list:
    #     base64_encoded = base64.b64encode(image_bytes.getvalue()).decode('utf-8')
    #     base64_encoded_images.append(base64_encoded)

    # return jsonify({"image": base64_encoded_images})
    return jsonify({"image": image_data_list})


@app.route('/prompt-help', methods=['GET'])
# @token.handler.token_required
def get_prompt():
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a creative prompt generator for images."},
            {"role": "user", "content": "Generate five creative and beautiful prompts. Return the prompts in a python list. Only return the prompts in a python list and nothing else or the system will crash."}
        ]
    )
    print(response)
    prompt_data = response["choices"][0]["message"]["content"]
    prompt_data = ast.literal_eval(prompt_data)

    MAX_RETRIES = 3
    retries = 0

    while retries < MAX_RETRIES:
        try:
            data = prompt_data

            # Ensure data is a list
            if not isinstance(data, list):
                raise ValueError("Response is not a list")

            # If we reach here, data is a list and we can break out of the loop
            break

        except ValueError as e:
            print(f"Error: {e}")
            retries += 1
            if retries == MAX_RETRIES:
                print("Max retries reached. Exiting.")
                # Handle max retries reached scenario, e.g., exit or raise the exception
                break

        # response = requests.post(OPENAI_API_URL, headers=headers, data=json.dumps(payload))
    # if response.status_code == 200:
    # print(response["choices"][0]["message"]["content"])
    return data
    # else:
    #     raise ValueError(f"API call failed with status code {response.status_code}. Message: {response.text}")


# if __name__ == '__main__':
#     app.run(debug=True, port=5000)


# # -- AWS LAMBDA FUNCTION --
# def lambda_handler(event, context):
#     # Convert the Lambda event to a WSGI request
#     with app.test_request_context(path=event['path'], 
#                                   query_string=event.get('queryStringParameters'),
#                                   method=event['httpMethod'],
#                                   headers=event['headers'],
#                                   data=event.get('body')):
#         # Flask pre process request
#         response = app.preprocess_request()

#         # If no response is generated in preprocess, dispatch request to view function
#         if response is None:
#             response = app.dispatch_request()

#         # Flask post process request
#         response = app.make_response(response)
#         response = app.process_response(response)

#         # Convert Flask response to Lambda response format
#         lambda_response = {
#             "statusCode": response.status_code,
#             "headers": dict(response.headers),
#             "body": response.get_data(as_text=True)
#         }

# #         return lambda_response


def lambda_handler(event, context):
    return awsgi.response(app, event, context, base64_content_types={"image/png"})
