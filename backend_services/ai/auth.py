import requests
import json
import boto3
from functools import wraps
from botocore.exceptions import ClientError
import jwt
from flask import request, jsonify


cognito_client = boto3.client('cognito-idp')
ssm_client = boto3.client('ssm')


class GetParams():

    def get_ssm_parameters(self, param_names):
        try:
            response = ssm_client.get_parameters(
                Names=param_names,
                WithDecryption=True  # Assuming you want to decrypt the values if they are encrypted
            )

            # Extract valid parameters
            parameters = {param['Name']: param['Value'] for param in response.get('Parameters', [])}

            # Check for invalid parameters
            invalid_params = response.get('InvalidParameters', [])
            if invalid_params:
                print(f"Invalid parameters: {invalid_params}")

            return parameters
        except ClientError as e:
            print(f"An error occurred: {e}")
            return None


class TokenHandler():

    def __init__(self,):
        get_params = GetParams()
        param_names = [
            'COGNITO_APP_CLIENT_ID',
            'COGNITO_POOL_ID',
            'COGNITO_REGION_NAME',
        ]
        param_values = get_params.get_ssm_parameters(param_names)
        self.COGNITO_POOL_ID = param_values['COGNITO_POOL_ID']
        self.REGION_NAME = param_values['COGNITO_REGION_NAME']
        self.COGNITO_APP_CLIENT_ID = param_values['COGNITO_APP_CLIENT_ID']

    def get_jwks(self):
        jwks_url = f"https://cognito-idp.{self.REGION_NAME}.amazonaws.com/{self.COGNITO_POOL_ID}/.well-known/jwks.json"
        response = requests.get(jwks_url)
        return response.json()["keys"]

    def get_public_key(self, token, jwks):
        headers = jwt.get_unverified_header(token)
        kid = headers["kid"]
        # Search the JWKS for the key with the matching kid
        key = next((key for key in jwks if key["kid"] == kid), None)
        if key:
            return jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(key))
        return None

    def token_required(self, f):
        @wraps(f)
        def decorated(*args, **kwargs):
            print(request.cookies)
            token = request.cookies.get('access_token')  # Or request.headers.get('Authorization')
            print('loooool', token)
            if not token:
                return jsonify({"message": "Token is missing!"}), 403
            try:
                jwks = self.get_jwks()
                public_key = self.get_public_key(token, jwks)
                if not public_key:
                    return jsonify({"message": "Unable to find appropriate key!"}), 403
                # Verify the JWT
                decoded_token = jwt.decode(token, public_key, algorithms=["RS256"], audience=self.COGNITO_APP_CLIENT_ID)
                email = decoded_token.get('email')
                if email:
                    # You can now use the email within your function, for example, by adding it to kwargs
                    kwargs['email'] = email
                else:
                    return jsonify({"message": "Email not found in token!"}), 403
                # jwt.decode(token, public_key, algorithms=["RS256"], audience=self.COGNITO_APP_CLIENT_ID)
            except jwt.ExpiredSignatureError:
                return jsonify({"message": "Token has expired!"}), 403
            except jwt.InvalidTokenError as e:
                return jsonify({"message": "Token is invalid!", "error": str(e)}), 403

            return f(*args, **kwargs)
        return decorated

    # def token_required(self, f):
    #     @wraps(f)
    #     def decorated(*args, **kwargs):
    #         token = request.cookies.get('access_token')
    #         # [TODO] USE AUD AND SIGN FOR SECURITY
    #         if not token:
    #             return jsonify(message="Token is missing!"), 403
    #         try:
    #             # Decode without verification to inspect the payload
    #             unverified_payload = jwt.decode(token, options={"verify_signature": False, "verify_aud": False})
    #             print("Unverified Payload:", unverified_payload)
    #             # Rest of your logic...
    #         except Exception as e:
    #             print("Error:", str(e))

            # # Get the JWKS
            # jwks = get_jwks()
            # print('JWKS',jwks)
            # # Extract the correct public key from the JWKS
            # public_key = get_public_key(token, jwks)
            # # print('PUBLIC_KEY',public_key)
            # if not public_key:
            #     return jsonify(message="Unable to find appropriate key!"), 403
            # # Verify the JWT
            # # data = jwt.decode(token, public_key, algorithms=["RS256"], audience=self.COGNITO_APP_CLIENT_ID)
            # data = jwt.decode(token, public_key, algorithms=["RS256"])
            # print('DATA',data)

        #     try:
        #         jwks = self.get_jwks()
        #         # Extract the correct public key from the JWKS
        #         public_key = self.get_public_key(token, jwks)
        #         if not public_key:
        #             return jsonify(message="Unable to find appropriate key!"), 403
        #         # Verify the JWT
        #         data = jwt.decode(token, public_key, algorithms=["RS256"])
        #         sub_code = data['sub']
        #     except jwt.ExpiredSignatureError:
        #         print('ExpiredSignatureError')
        #         return jsonify(message="Token has expired!"), 403
        #     except jwt.DecodeError:
        #         print('DecodeError')
        #         return jsonify(message="Token is invalid!"), 403
        #     except jwt.MissingRequiredClaimError as e:
        #         print('MissingRequiredClaimError')
        #         return jsonify(message=f"Token is missing the {e.claim} claim!"), 403
        #     except jwt.PyJWTError:
        #         print('PyJWTError')
        #         return jsonify(message="There was an error with the token!"), 403
        #     return f(*args, **kwargs)

        # return decorated
