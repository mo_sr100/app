import requests

# URL of the API endpoint
url = "https://lbitfkerkd.execute-api.eu-west-2.amazonaws.com/test/generate-image?input=cat&orientation=Square"

# Make the GET request to the URL
response = requests.get(url)
print(response)

# Check if the request was successful
if response.status_code == 200:
    # Assuming the API returns a JSON response with URLs to images
    data = response.json()
    print(data)
    images = data.get('image', [])

    for image in images:
        print("images", )

    # Loop through the images and print the URLs
    for i, image_url in enumerate(images, start=1):
        print(f"Image {i}: {image_url}")

        # Additional code to download or further process the images can go here

else:
    print(f"Failed to fetch images. Status code: {response.status_code}")
