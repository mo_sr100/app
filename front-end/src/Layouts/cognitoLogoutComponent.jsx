import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { logout } from './cognitoLogout';

function LogoutComponent() {
    const navigate = useNavigate();

    useEffect(() => {
        async function performLogout() {
            await logout();
            // navigate('/'); // Redirect to another page after logout if needed
        }

        performLogout();
    });

    return null; // or return a loading spinner or a "Logging out..." message
}

export default LogoutComponent;