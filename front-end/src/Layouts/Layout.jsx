import React from "react";
import { BrowserRouter as Router, HashRouter } from "react-router-dom";
import { Private } from "./Private/Private";
import { AppHeader } from './Header/AppHeader';
import { AppFooter } from './Footer/AppFooter';
import { ConfigProvider } from "antd";
import { CognitoLogin } from './cognitoLogin'
import { LogoutComponent } from './cognitoLogoutComponent'

export const Layout = () => {
    return (
        <>
            <ConfigProvider
                theme={{
                    token: {
                        colorPrimary: "green"
                    },
                }}
            >
                <HashRouter>
                    <AppHeader />
                    <Private />
                    <AppFooter />
                    <CognitoLogin/>
        <LogoutComponent/>

                </HashRouter>
            </ConfigProvider>
        </>
    );
};
