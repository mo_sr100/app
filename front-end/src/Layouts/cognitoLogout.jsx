export async function logout() {
    // First, make a request to your Flask backend to clear the cookies
    try {
        const response = await fetch('http://localhost:6000/logout', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include' // do i need?
        });

        if (response.ok) {
            console.log("Cookies cleared successfully");
        } else {
            console.error("Error clearing cookies");
        }
    } catch (error) {
        console.error("Error making request to logout endpoint:", error);
    }

    // Now proceed to Cognito logout
    const logoutUrl = `https://doodl.auth.eu-west-2.amazoncognito.com/logout?client_id=6r5g84m87gfcqsormc7jh8fdjv&logout_uri=http://localhost:3000/`;
    window.location.href = logoutUrl;
}
