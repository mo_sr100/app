import React from 'react'
import { Route, Routes } from 'react-router-dom';
import { AppRoutes } from '../../Routes/AppRoutes';

export const Private = () => {
    const isMobileScreen = () => {
        return window.innerWidth <= 767;
    };
    return (
        <div
            style={{
                marginTop: isMobileScreen() ? "" : '80px',
                marginBottom: '80px',
            }}>
            <Routes>
                {AppRoutes.map((route, k) => {
                    return (
                        <Route
                            key={k}
                            path={route.path}
                            element={<route.component />}
                        />
                    );
                })}
            </Routes>
        </div >
    )
}
