import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
// import Cookies from 'js-cookie';
// import { useAuth } from './AuthContext'; // Adjust the path to your AuthContext

function CognitoLogin() {
  const location = useLocation();
  const navigate = useNavigate();
  // const { login } = useAuth();

 
  useEffect(() => {
    // Get the authorization code from the query string
    const searchParams = new URLSearchParams(location.search);
    const code = searchParams.get('code');
    
    
    if (code) {
      // Now, exchange the code for tokens
      fetchTokens(code);
    }
  }, [location, navigate]);

  const fetchTokens = async (code) => {
    // Send the code to your backend

    const response = await fetch('http://localhost:5200/exchange-code', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        // 'Access-Control-Allow-Credentials': true,
      },
      mode: 'cors',
      credentials: 'omit',
      body: JSON.stringify({ code: code })
    });
  
    if (response.ok) {
      // login();
      // Redirect user to a protected page or the main app page
      navigate('/input');
    } else {
      // Handle errors
    }
  };


  return (
    <div>Loading...</div>
  );
}

export default CognitoLogin;
