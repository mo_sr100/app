import React, { useState } from 'react';
import { Layout, Menu, Drawer, Button } from 'antd';
import {
    HomeOutlined,
    InfoCircleOutlined,
    ShoppingCartOutlined,
    UserOutlined,
    SearchOutlined,
    MenuOutlined,
} from '@ant-design/icons';

const { Header } = Layout;

export const AppHeader = () => {
    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    const isMobileScreen = () => {
        return window.innerWidth <= 767;
    };

    return (
        <Header style={{ background: 'white', display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
            <div style={{ color: '#000', fontSize: '1.5rem', fontWeight: 'bold' }}>
                Logo
            </div>

            {isMobileScreen() ? (
                <>
                    <Button type="primary" onClick={showDrawer} icon={<MenuOutlined />} />
                    <Drawer
                        placement="right"
                        closable={false}
                        onClose={onClose}
                        visible={visible}
                    >
                        <Menu mode="vertical" defaultSelectedKeys={['startDoodling']} onClick={onClose}>
                            <Menu.Item key="explore">Explore</Menu.Item>
                            <Menu.Item key="startDoodling">Start doodling</Menu.Item>
                            <Menu.Item key="about">About</Menu.Item>
                            <Menu.Item key="profile">Login</Menu.Item>
                            <Menu.Item key="cart" icon={<ShoppingCartOutlined />}>Cart</Menu.Item>
                        </Menu>
                    </Drawer>
                </>
            ) : (
                <Menu mode="horizontal" defaultSelectedKeys={['startDoodling']} style={{ flexGrow: 1 }} disabledOverflow>
                    <Menu.Item key="explore">Explore</Menu.Item>
                    <Menu.Item key="startDoodling">Start doodling</Menu.Item>
                    <Menu.Item key="about">About</Menu.Item>
                </Menu>
            )}

            {!isMobileScreen() && (
                <Menu mode="horizontal" defaultSelectedKeys={['']} disabledOverflow style={{ flexShrink: 0 }}>
                    <Menu.Item key="profile">Login</Menu.Item>
                    <Menu.Item key="settings" icon={<ShoppingCartOutlined />}>Cart</Menu.Item>
                </Menu>
            )}
        </Header>
    );
};

export default AppHeader;
