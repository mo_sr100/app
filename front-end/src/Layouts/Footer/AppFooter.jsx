import React from 'react';
import { Layout, Menu } from 'antd';
import "./AppFooter.css"
const { Footer } = Layout;

export const AppFooter = () => {
    return (
        <Footer className='AppFooter' style={{ background: "#fff", marginTop: 'auto', paddingTop: '10px', paddingBottom: '10px', zIndex: 5 }}>
            <div>
                <Menu mode="horizontal" style={{ background: "none", border: "none" }}>
                    <Menu.Item key="privacy">Privacy</Menu.Item>
                    <Menu.Item key="terms">Terms</Menu.Item>
                    <Menu.Item key="support">Customer Support</Menu.Item>
                </Menu>
            </div>
        </Footer>
    );
};
