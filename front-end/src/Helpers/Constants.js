export const THEME_PRIMARY_COLOR = "#FFCC45";
export const THEME_BLUE_BACKGROUND = "#EBEFFE";
export const THEME_GREEN_COLOR = "#00979C";
export const THEME_SECONDARY_COLOR = "#565B5C";
export const THEME_TEXT_GREY_COLOR = "#475467";
