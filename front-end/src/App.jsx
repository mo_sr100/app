import './App.css'
import { Layout } from './Layouts/Layout'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { store } from './redux';
import { Provider } from 'react-redux';

const App = () => {
  return (
    <>
      <Provider store={store}>
        <Layout />
        <ToastContainer />
      </Provider>
    </>
  )
}

export default App
