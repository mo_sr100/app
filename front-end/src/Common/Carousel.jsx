import React, { useState } from 'react';
import { Button, Carousel } from 'antd';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';
import './CarouselComponent.css';

const CustomCarousel = ({ isImageChoosed, images }) => {


    const [currentSlide, setCurrentSlide] = useState(0);

    const goToPrevSlide = () => {
        setCurrentSlide((prev) => (prev > 0 ? prev - 1 : images.length - 1));
    };

    const goToNextSlide = () => {
        setCurrentSlide((prev) => (prev + 1) % images.length);
    };

    const isMobileScreen = () => {
        return window.innerWidth <= 767;
    }

    return (
        <div className="custom-carousel-container" style={{
            width: '70%'
        }}>
            <Carousel autoplay={false} dots={false} style={{ zIndex: 2 }}>
                {images.map((imageUrl, index) => (
                    <div key={index} style={{ textAlign: 'center', width: '100%' }} >
                        <img style={{ width: '100%', height: 'auto' }} src={images[currentSlide]} alt={`Slide ${index + 1}`} className="carousel-image" />
                    </div>
                ))}
            </Carousel>
            <div
                className="custom-arrow prev-arrow"
                style={{
                    backgroundImage: `url(${images[currentSlide - 1]})`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                    zIndex: 1,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    opacity: 0.8,
                    width: isMobileScreen() ? "100px" : '150px',
                    left: isMobileScreen() ? '-50px' : "-100px"

                }}
                onClick={goToPrevSlide}
            >
                <Button className='mr-5' type='primary' shape='circle' size='large'><LeftOutlined /></Button>
            </div>
            <div
                className="custom-arrow next-arrow"
                style={{
                    backgroundImage: `url(${images[(currentSlide + 1) % images.length]})`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                    zIndex: 1,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    opacity: 0.8,
                    width: isMobileScreen() ? "100px" : '150px',
                    right: isMobileScreen() ? '-50px' : "-100px"

                }}
                onClick={goToNextSlide}
            >
                <Button className='ml-5' type='primary' shape='circle' size='large'><RightOutlined /></Button>
            </div>
        </div>
    );
};

export default CustomCarousel;
