import React, { useState } from 'react';
import { Carousel, Card, Button } from 'antd';
import { RightOutlined, LeftOutlined } from '@ant-design/icons';

const CardCarousel = ({ images }) => {
    const [currentSlide, setCurrentSlide] = useState(0);
    const carouselRef = React.createRef();

    const renderCard = (image, index) => (
        <Card style={{ width: '100%', maxWidth: 300, margin: '0 auto' }}>
            <img src={image} alt={`Card ${index + 1}`} style={{ width: '100%', height: 'auto' }} />
        </Card>
    );

    const handleBeforeChange = (from, to) => {
        setCurrentSlide(to);
    };

    const handleNextClick = () => {
        carouselRef.current.next();
    };

    const handleBackClick = () => {
        carouselRef.current.prev();
    };

    const isMobileScreen = () => {
        return window.innerWidth <= 767;
    }

    return (
        <div className="card-carousel-container" style={{ position: 'relative' }}>
            <Carousel
                ref={carouselRef}
                dots={false}
                slidesToShow={isMobileScreen() ? 3 : 6}
                slidesToScroll={1}
                beforeChange={handleBeforeChange}
            >
                {images.map((image, index) => (
                    <div key={index}>{renderCard(image, index)}</div>
                ))}
            </Carousel>
            <div className="next-icon-container" style={{ position: 'absolute', left: 10, top: '50%', transform: 'translateY(-50%)', zIndex: 1 }}>
                <Button size='large' className="next-icon" type="primary" shape="circle" icon={<LeftOutlined />} onClick={handleBackClick} />
            </div>
            <div className="next-icon-container" style={{ position: 'absolute', right: 10, top: '50%', transform: 'translateY(-50%)', zIndex: 1 }}>
                <Button size='large' className="next-icon" type="primary" shape="circle" icon={<RightOutlined />} onClick={handleNextClick} />
            </div>
        </div>
    );
};

export default CardCarousel;
