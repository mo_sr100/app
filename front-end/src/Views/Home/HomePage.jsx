import React, { useState, useContext, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
// import { ImageContext } from './ImageContext';
import promptsData from './../../Helpers/prompts.json';
// import Typewriter from 'typewriter-effect/dist/core';
import SVGComponent from '../../assets/QuestionIcon';
import { QuestionCircleOutlined } from '@ant-design/icons';

// import "./HomePage.css"
import { Input, Button, Select, Spin, Typography, Image, Card, Row, Col, Tooltip, Divider } from 'antd';
import cat_surrealism from "../../assets/image_style/cat_surrealism.jpeg"
import cat_waterpaint from "../../assets/image_style/cat_waterpaint.jpeg"
import cat_abstract from "../../assets/image_style/cat_abstract.jpg"
import cat_anime from "../../assets/image_style/cat_anime.jpeg"
import cat_cubism from "../../assets/image_style/cat_cubism.jpeg"
import cat_cubism_ugly from "../../assets/image_style/cat_cubism_ugly.jpg"
import cat_gothic from "../../assets/image_style/cat_gothic.jpeg"
import cat_minimalism from "../../assets/image_style/cat_minimalism.jpeg"
import cat_modernart from "../../assets/image_style/cat_modernart.jpeg"
import cat_renaissance from "../../assets/image_style/cat_renaissance.jpeg"
import cat_impressionism from "../../assets/image_style/cat_impressionism.jpg"
import cat_oilpaint from "../../assets/image_style/cat_oilpaint.jpg"
import cat_origami from "../../assets/image_style/cat_origami.jpg"
import cat_popart from "../../assets/image_style/cat_popart.jpg"
import cat_realism from "../../assets/image_style/cat_realism.jpg"
import cat_pixelart from "../../assets/image_style/cat_pixelart.jpg"
import { Loading } from '../../Common/Loading';
import { GenericModal } from '../../Common/GenericModal';

// import portrait from "../../assets/portrait.jpg"
// import landscape from "../../assets/landscape.jpg"
// import square from "../../assets/square.jpg"

import { ROUTES_PATH } from '../../Routes/RoutesPath';
import { Start } from './Start';
import CardCarousel from '../../Common/CardComponents';
import AutoTypingInput from './AutoTypingInput';
import NoStyleLogo from '../../assets/nostyle.svg';
import one from '../../assets/landscape.svg';
import two from '../../assets/portrait.svg';
import three from '../../assets/square.svg';

const { Option } = Select;
const { Text } = Typography;

export const HomePage = () => {
    // const { setSelectedImage } = useContext(ImageContext);
    const [inputValue, setInputValue] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [generatedImageUrls, setGeneratedImageUrls] = useState([]);
    const [selectedImageUrl, setSelectedImageUrl] = useState(null);
    const [showConfirmButton, setShowConfirmButton] = useState(false);
    const navigate = useNavigate();
    const [promptHelpList, setPromptHelpList] = useState([]);
    const [selectedStyle, setSelectedStyle] = useState('');
    const [promptValue, setPromptValue] = useState('');

    const [selectedSize, setSelectedSize] = useState('');
    const [inputPlaceholder, setInputPlaceholder] = useState('');
    const typewriterRef = useRef(null);
    const [tabPressed, setTabPressed] = useState(false);
    const [fullPrompt, setFullPrompt] = useState('');
    const [styleImage, setStyleImage] = useState('');
    const [isHelpPromptOpen, setIsHelpPromptOpen] = useState(false)
    const [generatePromptValue, setGeneratePromptValue] = useState('')
    const [showPromptsDropDown, setShowPromptsDropDown] = useState(false)
    const [isImageGenerated, setIsImageGenerated] = useState(false)
    const [isImageChoosed, setIsImageChoosed] = useState(false)

    const imageSizeOptions = [
        { title: 'Square', imgUrl: '../../assets/square.jpg' },
        { title: 'Landscape', imgUrl: '../../assets/landscape.jpg' },
        { title: 'Portrait', imgUrl: '../../assets/portrait.jpg' }
      ];
      
    function sanitizeString(str) {
        const map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#x27;',
            "/": '&#x2F;',
        };
        const reg = /[&<>"'/]/ig;
        return str.replace(reg, (match) => map[match]);
    }

    const styles = [
        { label: "Realism", value: 'cat_realism' },
        { label: "Pop Art", value: 'cat_popart' },
        { label: "Pixel Art", value: 'cat_pixelart' },
        { label: "Minimalism", value: 'cat_minimalism' },
        { label: "Abstract", value: 'cat_abstract' },
        { label: "Cubism", value: 'cat_cubism' },
        { label: "Oil Paint", value: 'cat_oilpaint' },
        { label: "Water Paint", value: 'cat_waterpaint' },
        { label: "Origami", value: 'cat_origami' },
        { label: "Anime", value: 'cat_anime' },
        { label: "Impressionism", value: 'cat_impressionism' },
        { label: "Surrealism", value: 'cat_surrealism' },
    ];

    const prompts = [
        { label: "Travel Adventure", value: 'travel_adventure' },
        { label: "Dreamy Landscape", value: 'dreamy_landscape' },
        { label: "City Skylines", value: 'city_skylines' },
        { label: "Mystical Creatures", value: 'mystical_creatures' },
        { label: "Sci-Fi Futuristic", value: 'sci_fi_futuristic' },
        { label: "Underwater Fantasy", value: 'underwater_fantasy' },
        { label: "Cozy Winter Cabin", value: 'cozy_winter_cabin' },
        { label: "Whimsical Forest", value: 'whimsical_forest' },
        { label: "Time Travel Dilemma", value: 'time_travel_dilemma' },
        { label: "Celestial Wonder", value: 'celestial_wonder' },
        { label: "Steampunk Adventure", value: 'steampunk_adventure' },
        { label: "Abstract Mind Maze", value: 'abstract_mind_maze' },
    ];


    // useEffect(() => {
    //   // Your existing useEffect content

    //   // Disable right-click on the entire page
    //   const disableRightClick = (event) => {
    //     event.preventDefault();
    //   };

    //   document.addEventListener('contextmenu', disableRightClick);

    //   // Cleanup function to remove event listener
    //   return () => {
    //     document.removeEventListener('contextmenu', disableRightClick);
    //   };
    // }, []); // Ensure this useEffect hook runs only once on mount

    const isMobileScreen = () => {
        return window.innerWidth <= 767;
    };
    useEffect(() => {
        if (!typewriterRef.current) {
            const randomIndex = Math.floor(Math.random() * promptsData.length);
            const selectedPrompt = promptsData[randomIndex].prompt;
            setFullPrompt(selectedPrompt); // Store the full prompt immediately
            console.log(selectedPrompt)

            // typewriterRef.current = new Typewriter(null, {
            //     loop: false,
            //     delay: 75,
            //     onCreateTextNode: (char) => {
            //         // Directly update the state that controls the placeholder
            //         setInputPlaceholder((prev) => prev + char);
            //         return null; // Prevent the default behavior of appending nodes to the DOM
            //     },
            // });

            // typewriterRef.current.typeString(selectedPrompt).start();
        }

        return () => {
            // Clear the typewriter instance on component unmount
            if (typewriterRef.current) {
                typewriterRef.current.stop();
                typewriterRef.current = null;
            }
        };
    }, []); // Empty dependency array to ensure this effect runs only once on mount

    useEffect(() => {
        const handleKeyDown = (event) => {
            if (event.key === "Tab" && !tabPressed) {
                event.preventDefault(); // Prevent the default tab behavior
                setInputValue(fullPrompt); // Use the full prompt
                setTabPressed(true); // Indicate that Tab has been pressed
            }
        };

        document.addEventListener("keydown", handleKeyDown);

        return () => {
            document.removeEventListener("keydown", handleKeyDown);
        };
    }, [fullPrompt, tabPressed]); // Update the dependency array to include fullPrompt

    // useEffect(() => {
    //     const imageMap = {
    //         "Realism": "cat_realism.jpg",
    //         "Pop Art": "cat_popart.jpg",
    //         "Pixel Art": "cat_pixelart.jpg",
    //         "Minimalism": "cat_minimalism.jpeg",
    //         "Abstract": "cat_abstract.jpg",
    //         "Cubism": "cat_cubism.jpeg",
    //         "Oil Paint": "cat_oilpaint.jpg",
    //         "Water Paint": "cat_waterpaint.jpeg",
    //         "Origami": "cat_origami.jpg",
    //         "Anime": "cat_anime.jpeg",
    //         "Impressionism": "cat_impressionism.jpg",
    //         "Surrealism": "cat_surrealism.jpeg",
    //     };

    //     // Set the image path based on the selected style
    //     const imagePath = imageMap[selectedStyle] ? `image_style/${imageMap[selectedStyle]}` : '';
    //     setStyleImage(imagePath);
    // }, [selectedStyle]);


    const handleStyleButtonClick = (style) => {
        setSelectedStyle(prevStyle => prevStyle === style ? '' : style);
    };

    const handleSizeButtonClick = (size) => {
        setSelectedSize(prevSize => prevSize === size ? '' : size);
    };

    const handleGenerateClick = async () => {
        setIsLoading(true);
        // const finalInputValue = `${inputValue} ${selectedStyle}`.trim(); // Append style here for backend request
        const styleSegment = selectedStyle ? `${selectedStyle} style` : ''; // If a style is selected, append "style" to it
        const sizeSegment = selectedSize ? selectedSize : 'Square';
        const finalInputValue = `${inputValue} ${styleSegment}`.trim(); // Ensures no trailing spaces
        console.log(finalInputValue);

        try {
            let response = await fetch(`http://localhost:5000/generate-image?input=${sanitizeString(finalInputValue)}&orientation=${sizeSegment}`, {
                method: 'GET',
                credentials: 'include',
            });
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            const jsonResponse = await response.json();
            console.log("Server response:", jsonResponse);

            // Assuming the jsonResponse can either contain base64 encoded strings or URLs
            const images = jsonResponse.image; // Adjust this based on the actual structure of jsonResponse

            if (!images || !Array.isArray(images)) {
                throw new Error('Unexpected server response format');
            }

            const imageUrls = images.map(image => {
                // Check if the image string is a URL or a base64 encoded string
                if (image.startsWith('http')) {
                    console.log(image)
                    // If it's a URL, use it directly
                    return image;
                } else {
                    // If it's a base64 encoded string, convert it to a data URL
                    return `data:image/jpeg;base64,${image}`;
                    // console.log(image)
                    // return image
                }
            });

            setGeneratedImageUrls(prevUrls => [...prevUrls, ...imageUrls]);

        } catch (error) {
            console.error("There was an error fetching the images:", error);
        } finally {
            setIsLoading(false);
            setIsImageGenerated(true)
        }
    };


    function arrayBufferToBase64(buffer) {
        let binary = '';
        const bytes = [].slice.call(new Uint8Array(buffer));
        bytes.forEach((byte) => binary += String.fromCharCode(byte));
        return window.btoa(binary);
    }


    const handleImageSelect = (imageUrl) => {
        setSelectedImageUrl(imageUrl);
        // setSelectedImage(imageUrl);
        console.log(imageUrl)
        console.log(selectedImageUrl)
        setShowConfirmButton(true);
    };

    const handleConfirmClick = () => {
        console.log(selectedImageUrl)
        navigate("/photoeditor");
    };

    const handlePromptHelpClick = async () => {
        setIsHelpPromptOpen(true)
        // try {
        //     const response = await fetch('http://localhost:5000/prompt-help',
        //         { method: 'GET', credentials: 'include', });  // Adjust the endpoint path as needed
        //     const data = await response.json();
        //     setPromptHelpList(data);  // Assuming the Flask API returns a list of strings
        // } catch (error) {
        //     console.error('Error fetching prompt help:', error);
        // }
    };

    const handleListItemClick = (itemValue) => {
        setInputValue(itemValue);
    };

    const getImageOnStyle = () => {
        switch (selectedStyle) {
            case "cat_realism":
                return cat_realism;
            case "cat_popart":
                return cat_popart;
            case "cat_pixelart":
                return cat_pixelart;
            case "cat_minimalism":
                return cat_minimalism;
            case "cat_abstract":
                return cat_abstract;
            case "cat_cubism":
                return cat_cubism;
            case "cat_oilpaint":
                return cat_oilpaint;
            case "cat_waterpaint":
                return cat_waterpaint;
            case "cat_origami":
                return cat_origami;
            case "cat_anime":
                return cat_anime;
            case "cat_impressionism":
                return cat_impressionism;
            case "cat_surrealism":
                return cat_surrealism;
            default:
                return cat_abstract;
        }
    };


    const getImageOnSize = () => {
        switch (selectedSize) {
            case 'Square':
                return {
                    src: cat_surrealism,
                    width: '300px',
                    height: '300px',
                };
            case 'Landscape':
                return {
                    src: cat_surrealism,
                    width: '500px',
                    height: '300px',
                };
            case 'Portrait':
                return {
                    src: cat_surrealism,
                    width: '300px',
                    height: '400px',
                };
            default:
                return null;
        }
    };

    return (
        <>
            {
                isHelpPromptOpen && <GenericModal
                    visible={isHelpPromptOpen}
                    title={"Prompt Help"}
                    onCancel={() => setIsHelpPromptOpen(false)}
                    width={1000}
                    content={<>
                        <Row gutter={[16, 16]} className="mt-5 mb-5">

                            <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 18 }} lg={{ span: 18 }} xl={{ span: 18 }} className=''>
                                <div className="header-with-icon  ">
                                    <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                                        <h6>Enter some text - or just leave it blank</h6>
                                        <Tooltip title={<>These words can be anything, such as "an elephant in wellington boots", "happiness" or "aliens"... <br /><br /> Or leave it empty and DoodlBot will create a bunch of random prompts for you!</>}>
                                            <QuestionCircleOutlined style={{ fontSize: "18px" }} className="question-icon" />
                                        </Tooltip>
                                    </div>
                                    <AutoTypingInput inputValue={generatePromptValue} setInputValue={setGeneratePromptValue} />
                                </div>
                            </Col>
                            <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 6 }}>
                                <div className="d-flex justify-content-end align-items-start " style={{ marginTop: '' }}>
                                <Tooltip title={<>Hit the button below to see what DoodlBot has in store for you.</>}>
                                        <QuestionCircleOutlined style={{ fontSize: "18px" }} className="ml-2 question-icon" />
                                    </Tooltip>
                                </div>

                                <Button
                                    type="primary"
                                    className="" size='large'
                                    style={{ width: '100%', marginTop: '8px' }}
                                    onClick={() => setShowPromptsDropDown(true)}
                                    shape='round'
                                    disabled={generatePromptValue === ""}
                                >
                                    Generate Prompt
                                </Button>
                            </Col>
                        </Row>
                        {showPromptsDropDown &&
                            <>
                                <Divider />
                                <h6>Like any of these prompts?</h6>
                                <Select
                                    size={isMobileScreen() ? '' : 'large'}
                                    style={{ width: '100%', fontSize: isMobileScreen() ? "" : '20px' }}
                                    className="style-dropdown"
                                    value={promptValue}
                                    onChange={(value) => setPromptValue(value)}
                                >
                                    <Option value="">Select a prompt</Option>
                                    {prompts.map((prompt) => (
                                        <Option key={prompt.value} value={prompt.value}>
                                            {prompt.label}
                                        </Option>
                                    ))}
                                </Select>


                                <div className="mt-5 mb-4" style={{ textAlign: 'center' }}>
                                    <Button
                                        type="primary"
                                        className=""
                                        size='large'
                                        style={{ width: '50%', marginTop: '8px'}}
                                        onClick={() => {
                                            setInputValue(prompts?.find(prompt => prompt.value === promptValue)?.label || "")
                                            setIsHelpPromptOpen(false)
                                        }}
                                        shape='round'
                                        disabled={promptValue ? false : true}
                                    >
                                        Choose This Prompt
                                    </Button>
                                </div>

                            </>
                        }
                    </>}
                >
                </GenericModal >
            }

            <Row gutter={[16, 16]} style={{ width: '100%' }}>
                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 2 }} lg={{ span: 2 }} xl={{ span: 2 }}></Col>
                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 20 }} lg={{ span: 20 }} xl={{ span: 20 }} style={{ paddingRight: '0px' }}>
                    <div className={isMobileScreen() ? 'pl-3 pr-3' : ""} style={{ textAlign: 'left', backgroundColor: 'rgb(242, 242, 240)', width: '100%' }}
                    >
                        <h3>Image Studio</h3>
                        {
                            isMobileScreen() && <br />
                        }
                        <div className={isMobileScreen() ? "" : " p-5 "}>
                            <Row gutter={[16, 16]}>
                                <Col xs={{ span: 15 }} sm={{ span: 15 }} md={{ span: 18 }} lg={{ span: 18 }} xl={{ span: 18 }}>
                                    <div className="input-area">
                                        <div className="header-with-icon">
                                            <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                                                <h4>Type a Prompt</h4>
                                                <Tooltip title={<>Like this prompt? Just hit Tab to snap it up! <br /><br /> DoodlBot will use your prompt to create an image that matches your vision. <br /><br /> Feel free to get as detailed and creative as you want—we love seeing your imagination at work!</>}>
                                                    <QuestionCircleOutlined style={{ fontSize: "18px" }} className="question-icon" />
                                                </Tooltip>
                                            </div>
                                            <AutoTypingInput inputValue={inputValue} setInputValue={setInputValue} />


                                        </div>
                                    </div>
                                </Col>
                                <Col xs={{ span: 9 }} sm={{ span: 9 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 6 }}
                                >
                                    <div className="d-flex justify-content-end  " style={{ marginTop: isMobileScreen() ? "9px" : '8px' }}>
                                    <Tooltip title={<>Need some help coming up with a prompt? Let DoodlBot create one for you! <br /><br /> Don't worry, the prompts are super imaginative and detailed!</>}>
                                            <QuestionCircleOutlined style={{ fontSize: "18px" }} className="ml-2 question-icon" />
                                        </Tooltip>
                                    </div>

                                    <Button
                                        type="primary"
                                        className=""
                                        size={isMobileScreen() ? "small" : 'large'}
                                        style={{ width: isMobileScreen() ? "" : '100%', marginTop: '10px' }}
                                        onClick={handlePromptHelpClick}
                                        shape='round'
                                    >
                                        Prompt Help
                                    </Button>
                                </Col>
                            </Row>
                            <br />
                            <Divider />
                            <div className="size-area mt-3">
                                <Row gutter={[16, 16]}>
                                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 24 }} xl={{ span: 24 }}>
                                        <div className="header-with-icon">
                                            <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                                                <h4 className='mt-3'>Choose a Size</h4>
                                                <Tooltip title={<>Cropping will be available later to adjust your creation. <br /><br /> For posters, keep in mind that size selection is crucial as you typically wouldn't want to crop them once they've got a design. <br /><br /> For t-shirts, don’t sweat it - choose wisely now and tailor later!</>}>
                                                    <QuestionCircleOutlined style={{ fontSize: "18px" }} className="question-icon" />
                                                </Tooltip>
                                            </div>
                                        </div>
                                        <Row gutter={[16, 16]}>
                                            {[
                                                { title: 'Square', imgUrl: three },
                                                { title: 'Portrait', imgUrl: two },
                                                { title: 'Landscape', imgUrl: one }
                                            ].map((size, idx) => (
                                                <Col xs={{ span: 8 }} sm={{ span: 8 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} key={idx}
                                                    style={{ textAlign: 'center', padding: '10px' }}>
                                                    <Button
                                                        className="custom-image-button" // Use the custom CSS class
                                                        style={{
                                                            width: 'auto', // Allow the button to adjust to the image size
                                                            height: 'auto', // Allow the button to adjust to the image size
                                                            padding: 0, // Remove padding to let the image take up the entire button space
                                                            border: 'none', // Remove border
                                                            // backgroundColor: 'transparent', // Remove background
                                                        }}
                                                        type="text" // Use "text" type to remove default button styling
                                                        onClick={() => handleSizeButtonClick(size.title)}
                                                    >
                                                        <img
                                                            src={size.imgUrl}
                                                            alt={size.title}
                                                            draggable="false"
                                                            style={{
                                                                maxWidth: '100%', // Ensure the image is not larger than its container
                                                                height: 'auto', // Maintain aspect ratio
                                                            }}
                                                        />
                                                    </Button>
                                                </Col>
                                            ))}
                                        </Row>
                                    </Col>
                                </Row>
                            </div>
                            <Divider className='' />
                            <Row gutter={[16, 16]} className="style-dropdown-container mt-3">
                                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                                    <div>
                                        <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                                            <h4 className='mt-3'>Choose a Style (Optional)</h4>
                                            <Tooltip title={<>Use one of our pre-selected image styles to help DoodlBot create your design, but this is just optional</>}>
                                                <QuestionCircleOutlined style={{ fontSize: "18px" }} className="question-icon" />
                                            </Tooltip>
                                        </div>
                                        <Select
                                            size={isMobileScreen() ? '' : 'large'}
                                            style={{ width: '100%', fontSize: isMobileScreen() ? "" : '20px' }}
                                            className="style-dropdown"
                                            value={selectedStyle}
                                            onChange={(value) => setSelectedStyle(value)}
                                        >
                                            <Option value="">Choose a Style</Option>
                                            {styles.map((style) => (
                                                <Option key={style.value} value={style.value}>
                                                    {style.label}
                                                </Option>
                                            ))}
                                        </Select>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center', // Ensures content is always centered regardless of screen size
                                        marginTop: '45px',
                                    }}
                                >
                                    {/* Additional wrapper div for centering the image container */}
                                    <div style={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
                                        <div style={{
                                            display: 'inline-flex', // Wraps tightly around its content
                                            justifyContent: 'center', // Center horizontally
                                            alignItems: 'center', // Center vertically
                                            border: '2px solid black',
                                            borderRadius: '5px',
                                            maxWidth: '60%', // Keeps the border tight around the content
                                            height: 'auto', // Adjusts height based on the content
                                        }}>
                                            {selectedStyle ? (
                                                <Image
                                                    src={getImageOnStyle()}
                                                    alt={`${selectedStyle} Style Display`}
                                                    style={{ maxWidth: '100%', height: 'auto', objectFit: 'contain' }}
                                                />
                                            ) : (
                                                <img src={NoStyleLogo} alt="No Style Logo" style={{ maxWidth: '100%', height: 'auto' }} />
                                            )}
                                        </div>
                                    </div>
                                </Col>
                            </Row>                           
                            <div className="images-selection-area mt-5">

                                {isLoading ? (
                                    <Loading />
                                ) : (
                                    <div className="images-container">
                                        {generatedImageUrls.map((url, idx) => (
                                            <div
                                                key={idx}
                                                onClick={() => handleImageSelect(url)}
                                                className={`image-wrapper ${selectedImageUrl === url ? 'selected' : ''}`}
                                            >
                                                <Image className="image" src={url} alt="Generated Content" />
                                            </div>
                                        ))}
                                    </div>
                                )}
                            </div>
                            {
                                isImageGenerated && <>
                                    <Divider className='' />
                                    <br />
                                    <Start
                                        isImageChoosed={isImageChoosed}
                                        setIsImageChoosed={setIsImageChoosed} />
                                </>
                            }
                        </div >
                        <div className='container'>
                            <Divider />
                        </div>
                    </div >
                </Col>
                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 2 }} lg={{ span: 2 }} xl={{ span: 2 }}></Col>
            </Row>
            <Row gutter={[16, 16]} style={{ width: '100%' }}>
                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 24 }} xl={{ span: 24 }} style={{ textAlign: 'left' }}>
                    <h4 className=' ml-5 mb-3 '>Need Some Inspiration?</h4>
                    <CardCarousel
                        images={[
                            cat_surrealism,
                            cat_waterpaint,
                            cat_abstract,
                            cat_anime,
                            cat_cubism,
                            cat_cubism_ugly,
                            cat_gothic,
                            cat_minimalism,
                            cat_modernart,
                            cat_renaissance,
                            cat_impressionism,
                            cat_oilpaint,
                            cat_origami,
                            cat_popart,
                            cat_realism
                        ]} />

                </Col>
            </Row>

            <Row gutter={[16, 16]} style={{ width: '100%', textAlign: 'center' }} className='mt-4'>
                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 2 }} lg={{ span: 2 }} xl={{ span: 2 }}></Col>
                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 20 }} lg={{ span: 20 }} xl={{ span: 20 }}
                    style={{
                        marginLeft: isMobileScreen() && '15px'
                    }}>
                    <div className='container ' >
                        <Divider />
                    </div>
                    <Row gutter={[16, 16]} style={{ width: '100%', }} className='mt-4' >
                        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 6 }}> </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}
                            style={{ display: isMobileScreen() ? "" : 'flex', justifyContent: 'center', }}>
                            <Button
                                size={"large"}
                                style={{ width: "100%", }}
                                type="primary"
                                shape='round'
                                className="ai-button generate-image mb-3 mr-3"
                                onClick={handleGenerateClick}
                                disabled={!isLoading && inputValue && selectedSize ? false : true}>
                                <h5>Generate Images</h5>
                            </Button>
                            <Button
                                size={"large"}
                                style={{ width: "100%", }}
                                type="primary"
                                className="ai-button generate-image "
                                onClick={() => { }}
                                shape='round'
                                disabled={!isImageChoosed}>
                                <h5>Next Page</h5>
                            </Button>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 6 }}> </Col>
                    </Row>
                    <br />
                    <br />

                </Col>
                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 2 }} lg={{ span: 2 }} xl={{ span: 2 }}></Col>
            </Row>
        </>
    );


}