import React, { useState, } from 'react';
import { Button, } from 'antd';
import cat_surrealism from "../../assets/image_style/cat_surrealism.jpeg"
import cat_waterpaint from "../../assets/image_style/cat_waterpaint.jpeg"
import cat_abstract from "../../assets/image_style/cat_abstract.jpg"
import cat_anime from "../../assets/image_style/cat_anime.jpeg"
import cat_cubism from "../../assets/image_style/cat_cubism.jpeg"
import cat_cubism_ugly from "../../assets/image_style/cat_cubism_ugly.jpg"


import Carousel from '../../Common/Carousel';
import { Loading } from '../../Common/Loading';

export const Start = ({ isImageChoosed, setIsImageChoosed }) => {
    const [isLoading, setIsLoading] = useState(false);

    return (
        <>
            {
                isLoading && <Loading />
            }
            <div className="container ">
                <Carousel
                    isImageChoosed={isImageChoosed}
                    images={[
                        cat_surrealism,
                        cat_waterpaint,
                        cat_abstract,
                        cat_anime,
                        cat_cubism,
                        cat_cubism_ugly
                    ]}
                />
                <div className='mt-5' style={{ textAlign: 'center', width: '100%' }}>
                    <Button
                        size={"large"}
                        style={{ width: "200px" }}
                        type="primary"
                        className="ai-button generate-image"
                        shape='round'
                        onClick={() => {
                            setIsLoading(true)

                            setTimeout(() => {
                                setIsLoading(false)
                                setIsImageChoosed(true)
                            }, 1000);
                        }}
                    >
                        Choose this image
                    </Button>
                </div>
            </div >

        </>
    );


}