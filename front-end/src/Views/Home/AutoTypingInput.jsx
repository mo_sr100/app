import React, { useState, useEffect } from 'react';
import { Input } from 'antd';
import styled from 'styled-components';
import promptsData from './../../Helpers/prompts.json'; // Confirm this matches the path to your JSON data

const AutoTypingInput = ({ setInputValue, inputValue }) => {
    const [placeholder, setPlaceholder] = useState('');

    useEffect(() => {
        // Static version: Typing effect for the first item's prompt
        // If you wish to dynamically change the item (e.g., on click), use an additional state to cycle through promptsData
        const sentence = promptsData[0].prompt; // This will always pick the first sentence. Adjust index for different one or apply dynamic behavior as needed
        let charIndex = 0;

        const typeNextCharacter = () => {
            if (charIndex < sentence.length) {
                setPlaceholder(sentence.slice(0, charIndex + 1));
                charIndex += 1;
                setTimeout(typeNextCharacter, 100); // Increase the delay if typing effect seems too fast
            }
        };

        typeNextCharacter();

    }, []); // Empty dependency array to ensure this logic runs only once on mount

    const isMobileScreen = () => {
        return window.innerWidth <= 767;
    };

    return (
        <Input
            style={{ display: 'flex', alignItems: 'center', lineHeight: '1', fontSize: isMobileScreen() ? "" : '20px' }}
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
            placeholder={placeholder}
            size={isMobileScreen() ? "" : "large"}
        />
    );
};

export default AutoTypingInput;
