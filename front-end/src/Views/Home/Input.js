import React, { useState, useContext, useEffect, useRef } from 'react';
import { BeatLoader } from 'react-spinners';
import { useNavigate } from 'react-router-dom';
import { ImageContext } from './ImageContext';
import './input.css';
import Cookies from 'js-cookie';
import promptsData from './prompts/prompts.json';
import Typewriter from 'typewriter-effect/dist/core';
import SVGComponent from './components/QuestionIcon';

function ImageGenerator() {
  const { setSelectedImage } = useContext(ImageContext);
  const [inputValue, setInputValue] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [generatedImageUrls, setGeneratedImageUrls] = useState([]);
  const [selectedImageUrl, setSelectedImageUrl] = useState(null);
  const [showConfirmButton, setShowConfirmButton] = useState(false);
  const navigate = useNavigate();
  const [promptHelpList, setPromptHelpList] = useState([]);
  const [selectedStyle, setSelectedStyle] = useState('');
  const [isStyleSelected, setIsStyleSelected] = useState({});
  const [selectedSize, setSelectedSize] = useState('');
  const [inputPlaceholder, setInputPlaceholder] = useState('');
  const typewriterRef = useRef(null);
  const [tabPressed, setTabPressed] = useState(false);
  const [fullPrompt, setFullPrompt] = useState('');
  const [styleImage, setStyleImage] = useState('');


  function sanitizeString(str) {
    const map = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#x27;',
      "/": '&#x2F;',
    };
    const reg = /[&<>"'/]/ig;
    return str.replace(reg, (match) => map[match]);
  }

  const styles = [
    "Realism",
    "Pop Art",
    "Pixel Art",
    "Minimalism",
    "Abstract",
    "Cubism",
    "Oil Paint",
    "Water Paint",
    "Origami",
    "Anime",
    "Impressionism",
    "Surrealism",
  ];  


  // useEffect(() => {
  //   // Your existing useEffect content

  //   // Disable right-click on the entire page
  //   const disableRightClick = (event) => {
  //     event.preventDefault();
  //   };

  //   document.addEventListener('contextmenu', disableRightClick);

  //   // Cleanup function to remove event listener
  //   return () => {
  //     document.removeEventListener('contextmenu', disableRightClick);
  //   };
  // }, []); // Ensure this useEffect hook runs only once on mount

  
  useEffect(() => {
    if (!typewriterRef.current) {
      const randomIndex = Math.floor(Math.random() * promptsData.length);
      const selectedPrompt = promptsData[randomIndex].prompt;
      setFullPrompt(selectedPrompt); // Store the full prompt immediately
      console.log(selectedPrompt)

      typewriterRef.current = new Typewriter(null, {
        loop: false,
        delay: 75,
        onCreateTextNode: (char) => {
          // Directly update the state that controls the placeholder
          setInputPlaceholder((prev) => prev + char);
          return null; // Prevent the default behavior of appending nodes to the DOM
        },
      });

      typewriterRef.current.typeString(selectedPrompt).start();
    }

    return () => {
      // Clear the typewriter instance on component unmount
      if (typewriterRef.current) {
        typewriterRef.current.stop();
        typewriterRef.current = null;
      }
    };
  }, []); // Empty dependency array to ensure this effect runs only once on mount

  useEffect(() => {
    const handleKeyDown = (event) => {
      if (event.key === "Tab" && !tabPressed) {
        event.preventDefault(); // Prevent the default tab behavior
        setInputValue(fullPrompt); // Use the full prompt
        setTabPressed(true); // Indicate that Tab has been pressed
      }
    };
  
    document.addEventListener("keydown", handleKeyDown);
  
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, [fullPrompt, tabPressed]); // Update the dependency array to include fullPrompt
  
  useEffect(() => {
    const imageMap = {
      "Realism": "cat_realism.jpg",
      "Pop Art": "cat_popart.jpg",
      "Pixel Art": "cat_pixelart.jpg",
      "Minimalism": "cat_minimalism.jpeg",
      "Abstract": "cat_abstract.jpg",
      "Cubism": "cat_cubism.jpeg",
      "Oil Paint": "cat_oilpaint.jpg",
      "Water Paint": "cat_waterpaint.jpeg",
      "Origami": "cat_origami.jpg",
      "Anime": "cat_anime.jpeg",
      "Impressionism": "cat_impressionism.jpg",
      "Surrealism": "cat_surrealism.jpeg",
    };
  
    // Set the image path based on the selected style
    const imagePath = imageMap[selectedStyle] ? `image_style/${imageMap[selectedStyle]}` : '';
    setStyleImage(imagePath);
  }, [selectedStyle]);
    
  
  const handleStyleButtonClick = (style) => {
    setSelectedStyle(prevStyle => prevStyle === style ? '' : style);
  };
  
  const handleSizeButtonClick = (size) => {
    setSelectedSize(prevSize => prevSize === size ? '' : size);
  };
  
  const handleGenerateClick = async () => {
    setIsLoading(true);
    // const finalInputValue = `${inputValue} ${selectedStyle}`.trim(); // Append style here for backend request
    const styleSegment = selectedStyle ? `${selectedStyle} style` : ''; // If a style is selected, append "style" to it
    const sizeSegment = selectedSize ? selectedSize : 'Square';
    const finalInputValue = `${inputValue} ${styleSegment}`.trim(); // Ensures no trailing spaces
    console.log(finalInputValue);

    try {
      let response = await fetch(`http://localhost:5000/generate-image?input=${sanitizeString(finalInputValue)}&orientation=${sizeSegment}`, {
        method: 'GET',
        credentials: 'include',
      });
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const jsonResponse = await response.json();
      console.log("Server response:", jsonResponse);
  
      // Assuming the jsonResponse can either contain base64 encoded strings or URLs
      const images = jsonResponse.image; // Adjust this based on the actual structure of jsonResponse
  
      if (!images || !Array.isArray(images)) {
        throw new Error('Unexpected server response format');
      }
  
      const imageUrls = images.map(image => {
        // Check if the image string is a URL or a base64 encoded string
        if (image.startsWith('http')) {
          console.log(image)
          // If it's a URL, use it directly
          return image;
        } else {
          // If it's a base64 encoded string, convert it to a data URL
          return `data:image/jpeg;base64,${image}`;
          // console.log(image)
          // return image
        }
      });
  
      setGeneratedImageUrls(prevUrls => [...prevUrls, ...imageUrls]);
      
    } catch (error) {
        console.error("There was an error fetching the images:", error);
    } finally {
        setIsLoading(false);
    }
  };
  

  function arrayBufferToBase64(buffer) {
    let binary = '';
    const bytes = [].slice.call(new Uint8Array(buffer));
    bytes.forEach((byte) => binary += String.fromCharCode(byte));
    return window.btoa(binary);
  }


  const handleImageSelect = (imageUrl) => {
    setSelectedImageUrl(imageUrl);
    setSelectedImage(imageUrl);
    console.log(imageUrl)
    console.log(selectedImageUrl)
    setShowConfirmButton(true);
};

const handleConfirmClick = () => {
  console.log(selectedImageUrl)
  navigate("/photoeditor");
};

const handlePromptHelpClick = async () => {
  try {
    const response = await fetch('http://localhost:5000/prompt-help',
    {method: 'GET', credentials: 'include',});  // Adjust the endpoint path as needed
    const data = await response.json();
    setPromptHelpList(data);  // Assuming the Flask API returns a list of strings
  } catch (error) {
    console.error('Error fetching prompt help:', error);
  }
};

const handleListItemClick = (itemValue) => {
  setInputValue(itemValue);
};

  return (
    <div className="container">
      <div className="header-banner">
        <span>Doodl</span>
        <div className="nav-links">
          <a href="#">Explore</a>
          <a href="#" onClick={() => navigate('/start-doodling')}>Start Doodling</a>
          <a href="#">About</a>
        </div>
        <div className="auth-links">
          <a href="#">Login</a>
          <a href="#">Basket</a>
        </div>
      </div>
  
      <div className="grid-wrapper">
        <div className="input-area">
          <div className="header-with-icon">
            <h1>Type a prompt</h1>
            <div className="svg-tooltip-wrapper">
              <SVGComponent />
              <span className="tooltip-text">Like this prompt? Just hit Tab to snap it up!  <br /><br />
              The AI will use your prompt to create an image that matches your vision. Feel free to get as detailed and creative as you want—we love seeing your imagination at work!</span>
            </div>
          </div>
          <input
            className="input-field"
            type="text"
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
            placeholder={!tabPressed ? inputPlaceholder : ''}
          />
        </div>
  
        <button className="ai-button prompt-help" onClick={handlePromptHelpClick}>Prompt Help</button>
  
        <div className="size-area">
          <div className="header-with-icon">
            <h2>Choose a size</h2>
            <div className="svg-tooltip-wrapper">
              <SVGComponent className="svg-right-align" />
              <span className="tooltip-text">Cropping will be available later to adjust your creation. <br /><br /> For posters, keep in mind that size selection is crucial as you typically wouldn't want to crop them later. <br /><br /> For t-shirts, don’t sweat it - choose wisely now and tailor later!</span>
            </div>
          </div>
          <div className="sizes-buttons-row">
            {['Square', 'Landscape', 'Portrait'].map((size, idx) => (
              <button
                key={idx}
                className={`button size-button ${selectedSize === size ? 'selected' : ''}`}
                onClick={() => handleSizeButtonClick(size)}
              >
                {size}
              </button>
            ))}
          </div>
        </div>

        <div className="style-dropdown-container">
          {/* Column 1: Style Dropdown */}
          <div>
            <h2>Choose a style</h2>
            <select
              className="style-dropdown"
              value={selectedStyle}
              onChange={(e) => setSelectedStyle(e.target.value)}
            >
              <option value="">Select a style</option>
              {styles.map((style, idx) => (
                <option key={idx} value={style}>{style}</option>
              ))}
            </select>
          </div>
          
          {/* Column 2: SVG Component and Image Display */}
          <div>
            <div className="style-info-svg-align">
              <SVGComponent />
              <span className="tooltip-text">Your new tooltip text here</span>
            </div>            
            <div className="style-image-display">
              {styleImage && <img src={styleImage} alt={`${selectedStyle} Style Display`} />}
            </div>
          </div>
        </div>
  
        <div className="images-selection-area">
          <div className="header-with-icon">
            <h2>Select an Image:</h2>
            <SVGComponent className="svg-right-align" />
          </div>
          {isLoading ? (
            <BeatLoader color="#4A90E2" />
          ) : (
            <div className="images-container">
              {generatedImageUrls.map((url, idx) => (
                <div 
                  key={idx} 
                  onClick={() => handleImageSelect(url)} 
                  className={`image-wrapper ${selectedImageUrl === url ? 'selected' : ''}`}
                >
                  <img className="image" src={url} alt="Generated Content" />
                </div>
              ))}
            </div>
          )}
        </div>
  
        <button className="ai-button generate-image" onClick={handleGenerateClick} disabled={isLoading}>Generate Images</button>
        
        {/* {selectedImageUrl && (
          <button className="ai-button confirm" onClick={handleConfirmClick}>Confirm</button>
        )} */}
      </div>
  
      {/* {selectedImageUrl && (
        <div className="selected-image-section">
          <h2>Selected Image:</h2>
          <img src={selectedImageUrl} alt="Selected Content" style={{ maxWidth: '400px' }} />
          {showConfirmButton && (
            <button className="button" onClick={handleConfirmClick}>Confirm</button>
          )}
        </div>
      )} */}
    </div>
  );
  

}
export default ImageGenerator;