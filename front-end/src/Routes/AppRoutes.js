import React from "react";
import { ROUTES_PATH } from "./RoutesPath";
import { SignIn } from "../Views/Auth/SignIn/SignIn";
import { SignUp } from "../Views/Auth/SignUp/SignUp";
import { HomePage } from "../Views/Home/HomePage";
const AppRoutes = [
  {
    path: ROUTES_PATH.main,
    exact: true,
    name: "Home",
    component: HomePage,
    role: "COMMON",
  },
  {
    path: ROUTES_PATH.signIn,
    exact: true,
    name: "Login",
    component: SignIn,
    role: "COMMON",
  },
  {
    path: ROUTES_PATH.signUp,
    exact: true,
    name: "SignUp",
    component: SignUp,
    role: "COMMON",
  },
];

export { AppRoutes };
