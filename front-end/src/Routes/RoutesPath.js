export const ROUTES_PATH = {
  signIn: "/signin",
  signUp: "/signup",
  main: "/",
  explore: "/explore/",
  start: "/start/",
  about: "/about/",
  cart: "/cart",
};
