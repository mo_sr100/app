let initialState = {
  LoggedIn: false,
};
function userReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
export default userReducer;
