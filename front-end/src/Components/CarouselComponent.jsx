import { Carousel } from 'antd'
import React from 'react'
import ImagePreview from '../Common/ImagePreview'

export const CarouselComponent = ({ images, customHeight }) => {
    console.log("images", images)
    return (
        <Carousel
        >
            {images.map((image, index) => (
                <div key={index}>
                    <ImagePreview
                        uuId={image?.uuId}
                        styles={{
                            width: '100%',
                            height: customHeight || '520px',
                            objectFit: 'cover'
                        }}
                    />
                    {/* <img
                        style={{
                            width: '100%',
                            height: customHeight || '520px',
                            objectFit: 'cover'
                        }}
                        src={image} alt={`Car ${index + 1}`} /> */}
                </div>
            ))}
        </Carousel>
    )
}
