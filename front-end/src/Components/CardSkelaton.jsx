import React, { useState } from 'react';
import { Avatar, Card, Skeleton } from 'antd';
import "./CardSkelaton.css"
import Meta from 'antd/es/card/Meta';
const CardSkeleton = () => {
    const [active, setActive] = useState(true);

    return (
        <Card style={{
            background: 'rgb(245 245 245)'
        }}>

            <Skeleton.Image
                className='mb-2'
                active={active} />
            <Skeleton
                loading={true}
                avatar
                active
            >
                <Meta
                    avatar={
                        <Avatar
                            src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                        />
                    }
                    title="This is the description"
                    description="This is the description"
                />
            </Skeleton>
        </Card>
    );
};

export default CardSkeleton;
